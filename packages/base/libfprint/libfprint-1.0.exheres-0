# Copyright 2009,2014 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

UPLOADS_ID="aff93e9921d1cff53d7c070944952ff9"

require meson udev-rules

SUMMARY="Core library for the fprint stack"
HOMEPAGE="https://fprint.freedesktop.org"
DOWNLOADS="https://gitlab.freedesktop.org/${PN}/${PN}/uploads/${UPLOADS_ID}/${PNV}.tar.xz"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    gtk-doc
    fprint_drivers:
        (
            aes1610 [[ description = [ AuthenTec AES1610 ] ]]
            aes1660 [[ description = [ AuthenTec AES1660 ] ]]
            aes2501 [[ description = [ AuthenTec AES2501 ] ]]
            aes2550 [[ description = [ AuthenTec AES2550/AES2810 ] ]]
            aes2660 [[ description = [ AuthenTec AES1660 ] ]]
            aes3500 [[ description = [ AuthenTec AES3500 ] ]]
            aes4000 [[ description = [ AuthenTec AES4000 ] ]]
            elan [[ description = [ ElanTech Fingerprint Sensor ] ]]
            etes603 [[ description = [ EgisTec ES603 ] ]]
            upeksonly [[ description = [ UPEK TouchStrip sensor-only ] ]]
            upektc [[ description = [ UPEK TouchChip ] ]]
            upektc_img [[ description = [ Upek TouchChip Fingerprint Coprocessor ] ]]
            upekts [[ description = [ UPEK TouchStrip ] ]]
            uru4000 [[ description = [ Digital Personal U.are.U 4000/4000B ] ]]
            vcom5s [[ description = [ Veridicom 5thSense ] ]]
            vfs0050 [[ description = [ Validity VFS0050+ ] ]]
            vfs101 [[ description = [ Validity VFS101 ] ]]
            vfs301 [[ description = [ Validity VFS301/VFS300 ] ]]
            vfs5011 [[ description = [ Validity VFS5011 ] ]]
    ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc )
    build+run:
        dev-libs/glib:2[>=2.50]
        dev-libs/libusb:1[>=0.9.1]
        fprint_drivers:aes3500? ( x11-libs/pixman:1 )
        fprint_drivers:aes4000? ( x11-libs/pixman:1 )
        fprint_drivers:uru4000? ( dev-libs/nss )
"

src_configure() {
    for driver in ${FPRINT_DRIVERS} ; do
        drivers+=( ${driver} )
    done

    local meson_params=(
        -Ddrivers=$(IFS=, ; echo "${drivers[*]}")
        -Dgtk-examples=false
        -Dudev_rules=true
        -Dudev_rules_dir=${UDEVRULESDIR}
        -Dx11-examples=false
        $(meson_switch gtk-doc doc)
    )

    exmeson "${meson_params[@]}"
}

